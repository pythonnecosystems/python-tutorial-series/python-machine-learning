# Python 기계 학습: Scikit-learn 라이브러리 <sup>[1](#footnote_1)</sup>

> <font size="3">Python에서 scikit-learn 라이브러리를 사용하여 기계 학습 알고리즘을 구현하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./machine-learning.md#intro)
1. [Scikit-learn이란?](./machine-learning.md#sec_02)
1. [Scikit-Learn 설치와 임포트](./machine-learning.md#sec_03)
1. [데이터 준비와 전처리](./machine-learning.md#sec_04)
1. [Scikit-learn을 통한 지도 학습](./machine-learning.md#sec_05)
1. [Scikit-learn을 사용한 비지도 학습](./machine-learning.md#sec_06)
1. [모델 평가와 개선](./machine-learning.md#sec_07)
1. [요약](./machine-learning.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 46 — Python Machine Learning: Scikit-learn Library](https://python.plainenglish.io/python-tutorial-46-python-machine-learning-scikit-learn-library-3beed76d41d3?sk=72037e90122002aebe520181cffb3cc2)를 편역하였다.
