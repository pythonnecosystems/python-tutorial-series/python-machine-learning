# Python 기계 학습: Scikit-learn 라이브러리

## <a name="intro"></a> 개요
"Python 머신 러닝: Scikit-learn Library" 포스팅에서는 Python의 scikit-learn 라이브러리를 사용하여 머신 러닝 알고리즘을 구현하는 방법을 설명한다. 또한 데이터를 준비하고 전처리하는 방법, 모델을 평가하고 개선하는 방법, 지도 학습 및 비지도 학습과 같은 다양한 종류의 머신 러닝 기술을 적용하는 방법도 설명한다.

머신 러닝은 데이터로부터 학습하고 예측이나 결정을 내릴 수 있는 시스템을 만드는 것을 다루는 인공 지능의 한 분야이다. 머신 러닝은 자연어 처리, 컴퓨터 비전, 추천 시스템 등 다양한 영역에서 널리 사용되고 있다.

Python은 단순성, 가독성, 풍부한 라이브러리와 도구로 인해 기계 학습에 가장 많이 사용되는 프로그래밍 언어 중 하나이다. Python에서 기계 학습에 가장 널리 사용되는 라이브러리 중 하나는 scikit-learn으로 다양한 기계 학습 알고리즘과 작업할 수 있는 일관되고 사용자 친화적인 인터페이스를 제공한다.

scikit-learn은 NumPy, SciPy 및 matplotlib 위에 구축된 오픈 소스 라이브러리이다. 선형과 로지스틱 회귀 분석, 지원 벡터 머신, 의사 결정 트리, 랜덤 포레스트, k-means 클러스터링 등 기계 학습 알고리즘의 포괄적인 모음을 제공한다. 또한 데이터 전처리, 특징 추출, 모델 선택 및 평가를 위한 도구도 제공한다.

이 포스팅에서는 일반적인 기계 학습 문제를 해결하기 위해 scikit-learn을 사용하는 방법을 설명한다. Python과 데이터 분석에 대한 기본적인 이해와 함께 기계 학습 개념과 용어에 대한 숙지가 필요하다. 또한 Python 환경에 scikit-learn과 기타 필수 라이브러리를 설치하고 임포트하여야 한다.

이 포스팅을 학습하면 다음 작업을 수행할 수 있다.

- scikit-learn과 기타 필요한 라이브러리 설치와 임포트
- 머신 러닝을 위해 데이터 준비와 전처리
- 지도 학습 알고리즘을 적용한 분류와 회귀 작업
- 비지도 학습 알고리즘을 적용하여 클러스터링과 차원 축소 작업
- 다양한 메트릭과 기법을 사용하여 기계 학습 모델을 평가하고 개선힌다

여러분은 기계 학습 여정을 시작할 준비가 되었나요? 시작합시다!

## <a name="sec_02"></a> Scikit-learn이란?
scikit-learn은 데이터 분석을 위한 다양한 기계 학습 알고리즘과 도구를 제공하는 Python 라이브러리이다. 이는 다양한 타입의 데이터와 모델로 작업할 수 있는 일관되고 사용자 친화적인 인터페이스를 제공하기 때문에 Python에서 기계 학습을 위해 가장 인기 있고 널리 사용되는 라이브러리 중 하나이다.

scikit-learn은 원래 2007년 David Cournapeau가 Google Summer of Code 프로젝트의 일부로 개발하였다. 나중에, Fabian Pedregosa, Gael Baroquaux, Alexandre Gramfort 및 Olivier Grisel이 이끄는 개발자 팀에 의해 인수되었다. scikit-learn의 첫 번째 공개는 2010년이었고, 그 이후로, 많은 기여자 커뮤니티에 의해 지속적으로 업데이트되고 개선되어 왔다.

scikit-learn은 NumPy, SciPy 및 Matplotlib 세 Python 라이브러리 위에 구축되어 있다. NumPy와 SciPy는 배열, 행렬, 선형 대수, 최적화 및 통계 같은 수치와 과학 컴퓨팅을 위한 기본 기능을 제공한다. Matplotlib은 그래프, 차트 및 이미지와 같은 도표화와 시각화를 위한 기능을 제공한다. scikit-learn은 이러한 라이브러리를 사용하여 다양한 기계 학습 알고리즘과 데이터 구조를 구현하고 조작한다.

scikit-learning의 주요 특징과 이점은 다음과 같다.

- 선형과 로지스틱 회귀, 지원 벡터 머신, 의사 결정 트리, 랜덤 포레스트, k-means 클러스터링 등 포괄적인 기계 학습 알고리즘 모음을 제공한다.
- 스케일링, 인코딩, 대치, 교차 검증, 그리드 검색, 메트릭 등 데이터 전처리, 특징 추출, 모델 선택과 평가를 위한 도구를 제공한다.
- estimator, transformer predictor 및  predictor 객체를 사용하여 서로 다른 기계 학습 모델을 적용하고 비교할 수 있는 일관되고 간단한 인터페이스를 제공한다.
- 희박한 데이터 형식과 조밀 데이터 형식을 모두 지원하며, 수치 데이터와 범주형 데이터 모두를 처리할 수 있다.
- 문서화가 잘 되어 있고 사용자가 라이브러리를 배우고 사용할 수 있도록 많은 예시와 튜토리얼이 있다.
- 오픈 소스로 자유롭게 사용할 수 있으며, 개발자와 사용자들의 크고 활발한 커뮤니티가 있다.

이 포스팅에서는 몇 가지 일반적인 기계 학습 문제를 해결하기 위해 scikit-learn을 사용하는 방법을 설명한다. 또한 Python 환경에 scikit-learn과 기타 필수 라이브러리를 설치하고 임포트하는 방법도 다룬다.

## <a name="sec_02"></a> Scikit-Learn 설치와 임포트
scikit-learn을 사용하기 전에 컴퓨터에 설치해야 한다. scikit-learn을 설치하는 방법은 운영 체제와 Python 배포판에 따라 여러 가지가 있다. 이 포스팅에서는 컴퓨터에 Python 3.6 이상이 설치되어 있고 pip package Manager를 사용하여 Python 패키지를 설치한다고 가정한다.

pip을 사용하여 scikit-learn을 설치하려면 터미널 또는 명령 프롬프트 창을 열고 다음 명령을 입력해야 한다.

```bash
$ pip install scikit-learn
```

이렇게 하면 최신 버전의 scikit-learn과 NumPy 및 SciPy 같은 필요한 패키지도 즉 종속성을 함께 다운로드하여 설치할 수 있다. 패키지 이름 뒤에 버전 번호를 추가하여 특정 버전의 scikit-learn을 지정할 수도 있다. 예를 들어, 다음과 같이 할 수 있다.

```bash
$ pip install scikit-learn==0.24.2
```

이는 본 포스팅을 작성할 때 가장 안정적인 최신 버전인 scikit-learn 버전 0.24.2를 설치하게 된다. scikit-learn의 사용 가능한 버전은 [PyPI 홈페이지](https://pypi.org/)에서 확인할 수 있다.

scikit-learn 설치 중 문제나 오류가 발생할 경우 자세한 내용과 문제 해결 팁은 [공식 설치 가이드](https://pypi.org/project/scikit-learn/)를 참조하면 된다.

scikit-learn을 성공적으로 설치한 후 다음 문을 사용하여 Python 코드로 임포트할 수 있다.

```python
import sklearn
```

이렇게 하면 scikit-learn 라이브러리를 가져와 코드에서 사용할 수 있게 된다. 다음과 같이 scikit-learn에서 특정 모듈이나 기능을 임포트할 수도 있다.

```python
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
```

이렇게 하면 Linear_model과 metrics 모듈에서 각각 LinearRegression 클래스와 mean_squared_error 함수를 임포트한다. 별칭(alias)을 사용하여 임포트한 모듈이나 함수의 이름을 줄일 수도 있다. 예를 들어 다음과 같이 사용한다.

```python
import sklearn as sk
from sklearn.cluster import KMeans as KM
```

이렇게 하면 scikit-learne 라이브러리를 `sk`로, Kmeans 클래스를 `KM`으로 임포트하여 코드를 더 간결하고 작성하거나 읽을 수 있다.

이제 scikit-learn을 설치하고 임포트한 다음에는 scikit-learn의 기능을 탐색할 준비가 되었다. 다음 절에서는 기계 학습을 위해 데이터를 준비하고 전처리하는 방법을 설명할 것이다.

## <a name="sec_02"></a> 데이터 준비와 전처리
데이터 준비와 전처리는 모든 기계 학습 프로젝트에서 가장 중요한 단계 중 하나이다. 원시 데이터를 기계 학습 알고리즘에 적합하고 준비되도록 변형하고 클니닝하는 과정을 포함한다. 데이터 준비와 전처리는 기계 학습 모델의 성능과 정확성에 큰 영향을 미칠 수 있으므로 적절하고 신중하게 수행하는 것이 필수적이다.

이 절에서는 scikit-learn을 사용하여 다음과 같은 일반적인 데이터 준비와 전처리 작업을 수행하는 방법에 대해 알아본다.

- 데이터 로드와 탐색
- 데이터를 훈련과 테스트 세트로 분할
- 결측값과 이상치 처리
- 범주형 피쳐 인코딩
- 수치 피처의 스케일링과 정규화
- 차원 축소와 피처 선택

이 포스팅에서는 Boston Housing Dataset라고 불리는 sikit-learn의 샘플 데이터세트를 사용할 것이다. 이 데이터세트에는 방 갯수, 범죄율, 도심까지의 거리 및 집 값의 중간값 같은 보스턴 지역의 약 506채의 집에 대한 정보가 포함되어 있다. 이 데이터를 사용하여 다른 피처를 기반으로 집 값의 중앙값을 예측하는 것이 목표이다.

다음과 같이 `sklearn.datasets` 모듈에서 `load_boston` 함수를 사용하여 이 데이터세트를 로드할 수 있다.

```python
from sklearn.datasets import load_boston
boston = load_boston()
```

이렇게 하면 데이터세트에 대한 데이터, 타겟 및 일부 메타데이터를 포함하는 사전과 같은 객체가 반환된다. 데이터와 타겟에 각각 `'data'`와 `'target'` 키를 사용하여 액세스할 수 있다. 각각 `'feature_names'`와 `'DESCR'` 키를 사용하여 데이터세트의 특징 이름과 설명을 액세스할 수도 있다.

데이터를 탐색하려면 데이터를 쉽게 조작하고 분석할 수 있는 표 형식의 데이터 구조인 pandas DataFrame으로 변환하면 된다. pandas를 `pd`로 임포트하여 `pd.DataFrame` 함수를 사용하여 다음과 같이 데이터와 피처 이름으로부터 DataFrame을 만들 수 있다.

```python
import pandas as pd
X = pd.DataFrame(boston.data, columns=boston.feature_names)
y = pd.Series(boston.target, name='MEDV')
```

이렇게 하면 피쳐를 포함하는 `x`라는 DataFrame과 타겟을 포함하는 `y`라는 Series가 생성된다. Series는 모든 타입의 데이터를 저장할 수 있는 1차원 배열과 같은 개체이다. `name` 매개 변수를 사용하여 Series에 이름을 할당할 수 있으며 이 이름은 DataFrame과 결합할 때 열 이름으로 사용된다.

타겟을 피처와 결합하려면 지정된 축을 따라 두 개 이상의 pandas 객체를 연결하는 `pd.concat` 함수를 사용한다. 축 매개 변수를 사용하여 행(axis=0) 또는 열(axis=1)을 따라 연결할지 지정할 수 있다. 이 경우 열을 따라 연결하려면 다음과 같이 `axis=1`로 설정해야 한다.

```python
df = pd.concat([X, y], axis=1)
```

이렇게 하면 피처와 타겟을 모두 포함하는 `df`라는 새로운 DataFrame이 생성된다. `head` 메서드를 사용하여 다음과 같이 DataFrame의 처음 다섯 행을 출력할 수 있다.

```python
df.head()
```

이렇게 하면 다음과 같이 출력된다.

```
CRIM    ZN  INDUS  CHAS    NOX     RM   AGE     DIS  RAD    TAX  PTRATIO       B  LSTAT  MEDV
0  0.00632  18.0   2.31   0.0  0.538  6.575  65.2  4.0900  1.0  296.0     15.3  396.90   4.98  24.0
1  0.02731   0.0   7.07   0.0  0.469  6.421  78.9  4.9671  2.0  242.0     17.8  396.90   9.14  21.6
2  0.02729   0.0   7.07   0.0  0.469  7.185  61.1  4.9671  2.0  242.0     17.8  392.83   4.03  34.7
3  0.03237   0.0   2.18   0.0  0.458  6.998  45.8  6.0622  3
```

## <a name="sec_02"></a> Scikit-learn을 통한 지도 학습
지도 학습(supervised learning)은 레이블이 지정된 데이터, 즉 알려진 출력 또는 목표 변수를 가진 데이터로부터 학습하는 것을 포함하는 기계 학습의 한 타입이다. 지도 학습의 목표는 학습 데이터로부터 학습된 패턴에 기초하여 새로운 데이터 또는 보이지 않는 데이터에 대한 목표 변수를 예측할 수 있는 모델을 훈련하는 것이다.

지도학습에는 크게 분류와 회귀 분석의 두 가지 종류가 있다. 분류는 이메일이 스팸인지 아닌지, 종양이 양성 또는 악성인지와 같이 이산적이거나 범주인 목표 변수를 예측하는 작업이다. 회귀 분석은 주택가격이나 사람의 키와 같이 연속적이거나 수치인 목표 변수를 예측하는 작업이다.

scikit-learn은 분류 작업과 회귀 작업을 모두 처리할 수 있는 다양한 지도 학습 알고리즘을 제공한다. 가장 일반적이고 널리 사용되는 알고리즘은 다음과 같다.

- 선형(linear)과 로지스틱 회귀(logistic regression) 분석
- 서포트 벡터 머신(suuport vector machine)
- K-근접 이웃(K-nearest neighbors)
- 의사결정 나무(decision trees)와 랜덤 포레스트(random forests)
- 신경망(neural networks)

이 모든 알고리즘은 scikit-learn에서 estimator 객체로 구현되며, 이들은 서로 다른 모델을 적용하고 비교하기 위한 일관되고 간단한 인터페이스를 따른다. estimator 객체를 사용하기 위한 기본 단계는 다음과 같다.

1. estimator 클래스의 인스턴스를 만들고 선택적으로 모델의 하이퍼파라미터를 지정한다.
1. fit 메서드을 사용하여 모델을 훈련 데이터에 적합시키고, 선택적으로 목표 변수를 인수로 전달한다.
1. predict 메서드를 사용하여 새로운 데이터 또는 보이지 않는 데이터에 대한 목표 변수를 예측하고, 선택적으로 새로운 데이터의 피처를 인수로 전달한다.
1. score 메서드, 혼동 행렬(confusion matrix), 평균 제곱 오차(mean square error) 등 다양한 메트릭과 기법을 사용하여 모델의 성능과 정확도를 평가한다.

이 절에서는 scikit-learn을 사용하여 다음과 같은 일부 지도 학습 작업을 수행하는 방법을 설명한다.

- Boston Housing Dataset에 대한 선형 회귀 분석 수행
- Iris Dataset에 대한 로지스틱 회귀 분석 수행
- Bresst Cancer Dataset에 대한 서포트 벡터 분류 수행
- Digits Dataset에서 k-최근접 이웃 분류 수행
- Diabetes Dataset에 대한 의사 결정 트리 회귀 분석 수행
- 와인 데이터세트에서 랜덤 포레스트 분류 수행
- California Housing Dataset에 대한 신경망 회귀 분석 수행

각 작업에 대해 scikit-learn에서 제공하는 샘플 데이터세트를 사용할 것이며, 이 데이터세트는 `sklearn.datasets` 모듈의 `load_*` 함수를 사용하여 로드할 수 있다. 또한 `sklearn.model_selection` 모듈의 `train_test_split` 함수를 사용하여 데이터를 훈련과 테스트 세트로 분할하고 메트릭 모듈을 사용하여 모델을 평가할 것이다.

첫 번째 과제인 보스턴 주택 데이터세트에 대한 선형 회귀 분석을 수행하는 것부터 시작한다.

### Boston Housing Dataset에 대한 선형 회귀 분석 수행
Boston Housing Datase은 회귀 작업에 사용되는 대표적인 데이터세트이다. 범죄율, 방 갯수 등 다양한 요인을 바탕으로 보스턴의 주택 가격에 대한 정보를 담고 있다. 이 데이터세트에 대해 scikit-learn을 사용하여 선형 회귀를 수행할 수 있는 방법은 다음과 같다.

```python
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

# Load the Boston Housing Dataset
boston = load_boston()
X = boston.data
y = boston.target
# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# Create a linear regression model
model = LinearRegression()
# Train the model on the training data
model.fit(X_train, y_train)
# Make predictions on the testing data
y_pred = model.predict(X_test)
# Evaluate the model
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)
```

이 예에서는 먼저 `load_boston()`을 사용하여 Boston Housing Datase를 로드합니다. 그런 다음 `train_test_split()`을 사용하여 데이터를 훈련과 테스트 세트로 나눈다. 다음으로 `LinearRegression` 모델을 만들고 `fit()`을 사용하여 훈련 데이터에 대해 훈련시킨다. 그런 다음 훈련된 모델을 사용하여 테스트 데이터에 대한 예측을 수행하고 평균 제곱 오차(`mean_square_error()`)를 사용하여 성능을 평가한다.

데이터세트와 그에 따른 모델을 교체하여 scikit-learn을 사용하여 다른 지도 학습 작업을 유사하게 수행할 수 있다.

### Iris Dataset에 대한 로지스틱 회귀 분석 수행
Iris Dataset는 분류 작업에 사용되는 대표적인 데이터세트이다. iris 꽃의 세 종과 꽃잎의 길이와 너비에 대한 정보를 포함하고 있다. 이 데이터세트에 대해 `scikit-learn`을 사용하여 로지스틱 회귀 분석을 수행하는 방법은 다음과 같다.

```python
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report

# Load the Iris Dataset
iris = load_iris()
X = iris.data
y = iris.target
# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# Create a logistic regression model
model = LogisticRegression(max_iter=200)
# Train the model on the training data
model.fit(X_train, y_train)
# Make predictions on the testing data
y_pred = model.predict(X_test)
# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)
# Print classification report
print("Classification Report:")
print(classification_report(y_test, y_pred, target_names=iris.target_names))
```

이 예에서는 `load_iris()`를 사용하여 Iris Dataset를 로드하고 데이터를 훈련과 테스트 세트로 분할한 다음 로지스틱 회귀 모델을 만든다. 그런 다음 훈련 데이터에 대해 모델을 훈련하고 테스트 데이터에 대해 예측을 수행하고 정확도 점수(`accuracy_score()`)와 분류 보고서(`classification_report()`)를 사용하여 모델의 성능을 평가한다.

### Bresst Cancer Dataset에 대한 서포트 벡터 분류 수행
Bresst Cancer Dataset는 이진 분류 작업에 일반적으로 사용되는 또 다른 데이터세트이다. 유방 종괴의 세침 흡인액(FNA, fine needle aspirate)을 디지털화한 이미지에서 계산한 피처가 포함되어 있다. 이 작업은 종괴가 양성인지 악성인지를 예측하는 것다. 다음은 scikit-learn을 사용하여 이 데이터세트에 대한 서포트 벡터 분류를 수행하는 방법이다.

```python
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, classification_report

# Load the Breast Cancer Dataset
cancer = load_breast_cancer()
X = cancer.data
y = cancer.target
# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# Create a support vector classification model
model = SVC()
# Train the model on the training data
model.fit(X_train, y_train)
# Make predictions on the testing data
y_pred = model.predict(X_test)
# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)
# Print classification report
print("Classification Report:")
print(classification_report(y_test, y_pred, target_names=cancer.target_names))
```

이 예에서는 `load_breast_cancer()`를 사용하여 Bresst Cancer Dataset를 로드하고 데이터를 훈련과 테스트 세트로 나누고 `SVC`(Support Vector Classification) 모델을 만든다. 그런 다음 훈련 데이터로 모델을 훈련하고 테스트 데이터를 예측하고 정확도 점수와 분류 보고서를 사용하여 모델의 성능을 평가한다.

### Digits Dataset에서 k-최근접 이웃 분류 수행
Digits Dataset은 분류 작업에 일반적으로 사용되는 손으로 쓴 숫자(0~9)의 데이터세트이다. 각 데이터 포인트는 숫자의 8x8 이미지이며, 작업은 이미지가 어떤 숫자를 나타내는지 예측하는 것이다. scikit-learn을 사용하여 이 데이터세트에 대해 KNN(K-Nearest Neighbors) 분류를 수행하는 방법은 다음과 같다.

```python
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, classification_report

# Load the Digits Dataset
digits = load_digits()
X = digits.data
y = digits.target
# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# Create a K-Nearest Neighbors classification model
model = KNeighborsClassifier()
# Train the model on the training data
model.fit(X_train, y_train)
# Make predictions on the testing data
y_pred = model.predict(X_test)
# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)
# Print classification report
print("Classification Report:")
print(classification_report(y_test, y_pred))
```

이 예에서는 `load_digits()`를 사용하여 Digits Dataset을 로드하고 데이터를 훈련과 테스트 세트로 분할한 다음 `KNeighborsClassifier` 모델을 만듭니다. 그런 다음 훈련 데이터에 대해 모델을 훈련하고 테스트 데이터에 대해 예측하고 정확도 점수와 분류 보고서를 사용하여 성능을 평가한다.

### Diabetes Dataset에 대한 의사 결정 트리 회귀 분석 수행
Diabetes Dataset은 회귀 작업에 일반적으로 사용되는 데이터세트이다. 당뇨병 환자 442명을 대상으로 나이, 성별, 체질량지수(BMI), 혈압 등 10개의 기준 변수와 6개의 혈청 측정치가 포함되어 있다. 목표 변수는 기준 시점으로부터 1년 후의 질병 진행을 정량적으로 측정한 것이다. scikit-learn을 사용하여 이 데이터세트에 대한 의사결정 나무 회귀를 수행할 수 있는 방법은 다음과 같다.

```python
from sklearn.datasets import load_diabetes
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error

# Load the Diabetes Dataset
diabetes = load_diabetes()
X = diabetes.data
y = diabetes.target
# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# Create a decision tree regression model
model = DecisionTreeRegressor()
# Train the model on the training data
model.fit(X_train, y_train)
# Make predictions on the testing data
y_pred = model.predict(X_test)
# Evaluate the model
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)
```

이 예에서는 `load_diabetes()`를 사용하여 Diabetes Dataset를 로드하고 데이터를 훈련과 테스트 세트로 나누고 `DecisionTreeRegressor` 모델을 만든다. 그런 다음 훈련 데이터에 대해 모델을 훈련하고 테스트 데이터를 예측하고 평균 제곱 오차를 사용하여 성능을 평가한다.

### Wine Dataset에서 랜덤 포레스트 분류 수행
Wine Dataset는 분류 작업에 일반적으로 사용되는 데이터세트이다. 이탈리아의 같은 지역에서 재배되지만 세 가지 다른 품종에서 파생된 와인에 대한 화학적 분석 결과를 포함한다. 작업은 와인의 화학적 특성을 기반으로 와인의 품종을 예측하는 것이다. scikit-learn을 사용하여 이 데이터세트에 대해 랜덤 포레스트 분류를 수행하는 방법은 다음과 같다.

```python
from sklearn.datasets import load_wine
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report

# Load the Wine Dataset
wine = load_wine()
X = wine.data
y = wine.target
# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# Create a random forest classification model
model = RandomForestClassifier()
# Train the model on the training data
model.fit(X_train, y_train)
# Make predictions on the testing data
y_pred = model.predict(X_test)
# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)
# Print classification report
print("Classification Report:")
print(classification_report(y_test, y_pred, target_names=wine.target_names))
```

이 예에서는 `load_wine()`을 사용하여 Wine Dataset를 로드하고 데이터를 훈련과 테스트 세트로 나누고 `RandomForestClassifier` 모델을 만든다. 그런 다음 훈련 데이터에 대해 모델을 훈련하고 테스트 데이터에 대해 예측하고 정확도 점수와 분류 보고서를 사용하여 성능을 평가한다.

### California Housing Dataset에 대한 신경망 회귀 분석 수행
California Housing Dataset은 회귀 작업에 일반적으로 사용되는 데이터세트이다. 중위 소득, 중위 주택 값 및 캘리포니아 블록 그룹에 대한 기타 정보 같은 피처를 포함한다. 작업은 연속형 수치인 캘리포니아 지구에 대한 중위 주택 값을 예측하는 것이다. 이 데이터세트에 대해 scikit-learn을 사용하여 신경망 회귀를 수행하는 방법은 다음과 같다.

```python
from sklearn.datasets import fetch_california_housing
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

# Load the California Housing Dataset
california_housing = fetch_california_housing()
X = california_housing.data
y = california_housing.target
# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
# Create a neural network regression model
model = MLPRegressor(hidden_layer_sizes=(100, 50), max_iter=500)
# Train the model on the training data
model.fit(X_train, y_train)
# Make predictions on the testing data
y_pred = model.predict(X_test)
# Evaluate the model
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)
```

이 예에서는 `fetch_california_housing()`을 사용하여 California Housing Dataset를 로드하고 데이터를 훈련과 테스트 세트로 나누고 `MLPRegressor`(Multi-layer Perceptron Regressor) 모델을 만든다. 그런 다음 훈련 데이터에 대해 모델을 훈련하고 테스트 데이터에 대해 예측하고 평균 제곱 오차를 사용하여 성능을 평가한다.

## <a name="sec_02"></a> Scikit-learn을 사용한 비지도 학습
비지도 학습(unsupervised machine learning)은 기계 학습의 한 종류로 레이블이 지정되지 않은 데이터, 즉 알려진 출력이나 목표 변수가 없는 데이터로부터 학습하는 것이다. 비지도 학습의 목표는 사전 지식이나 안내 없이 데이터의 기본 구조와 패턴을 발견하는 것이다.

비지도 학습에는 크게 클러스터링과 차원 축소의 두 가지 종류가 있다. 클러스터링은 유클리드 거리, 코사인 유사도 또는 자카드 지수와 같은 유사성 또는 거리의 일부 측정값을 기반으로 유사한 데이터 포인트를 함께 그룹화하는 작업이다. 차원 축소는 분산, 상관관계 또는 상호 정보와 같은 정보를 최대한 보존하면서 데이터의 특징이나 차원의 수를 줄이는 작업이다.

scikit-learn은 클러스터링과 차원 축소 작업을 모두 처리할 수 있는 다양한 비지도 학습 알고리즘을 제공한다. 가장 일반적이고 널리 사용되는 알고리즘은 다음과 같다.

- K-평균 군집화(k-means clustering)
- 계층적 클러스터링
- DBSCAN 클러스터링
- 주성분 분석(principal component analysis)
- 특이치 분해(singular value decomposition)
- t-distributed 확률적 이웃 임베딩(stochasitic neighbor embedding)

이 모든 알고리즘은 scikit-learn에서 estimator 객체로 구현되며, 이들은 서로 다른 모델을 적용하고 비교하기 위한 일관되고 간단한 인터페이스를 따른다. estimator 객체를 사용하기 위한 기본 단계는 다음과 같다.

1. estimator 클래스의 인스턴스를 만들고 선택적으로 모델의 하이퍼파라미터를 지정한다.
1. fit 메서드를 사용하여 데이터에 model을 적합시키고, 선택적으로 목표 변수를 인수로 전달한다.
1. 변환 방법을 사용하여 데이터를 변환하고, 선택적으로 새로운 데이터의 피처를 인수로 전달한다.
- 실루엣(silhouette) 점수, Calinski-Harabasz 지수, Davies-Bouldin 지수 등 다양한 메트릭과 기법을 사용하여 모델의 성능과 품질을 평가한다.

이 절에서는 scikit-learn을 사용하여 다음과 같은 일부 비지도 학습 작업을 수행하는 방법에 대해 설명한다.

- Iris Dataset에 대한 k-means 클러스터링 수행
- Digits Dataset에서 계층적 클러스터링 수행
- Wine Dataset에서 DBSAN 클러스터링 수행
- Bresst Cancer Dataset에 대한 주성분 분석 수행
- Boston Housing Dataset에 대한 특이값 분해(singular value decomposition) 수행

각 작업에 대해 scikit-learn에서 제공하는 샘플 데이터세트를 사용하게 되며, 이 데이터세트는 `sklearn.datasets` 모듈에서 `load_*` 함수를 사용하여 로드할 수 있다. 또한 메트릭 모듈을 사용하여 모델을 평가할 수도 있다.

첫 번째 작업인 Iris Dataset에서 k-평균 클러스터링을 수행하는 것부터 시작한다.

### Iris Dataset에 대한 k-means 클러스터링 수행
Iris Dataset는 클러스터링과 분류 작업에 일반적으로 사용되는 데이터세트이다. 여기에는 각 iris의 종과 함께 팔 길이, 팔 너비, 꽃잎 길이 및 꽃잎 너비를 포함한 iris 꽃의 측정값이 포함되어 있다. 그러나 클러스터링 목적으로 종 레이블을 무시하고 꽃 측정값을 기반으로 데이터를 클러스터링하는 데 중점을 둘 것이다. 다음은 scikit-learn을 사용하여 이 데이터 세트에서 k-평균 클러스터링을 수행하는 방법이다.

```python
from sklearn.datasets import load_iris
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
import matplotlib.pyplot as plt

# Load the Iris Dataset
iris = load_iris()
X = iris.data
# Perform k-means clustering with different values of k
k_values = range(2, 6)
silhouette_scores = []
for k in k_values:
    # Create a k-means clustering model
    model = KMeans(n_clusters=k, random_state=42)
    
    # Fit the model to the data
    model.fit(X)
    
    # Calculate the silhouette score
    silhouette_scores.append(silhouette_score(X, model.labels_))
# Plot the silhouette scores for different values of k
plt.figure(figsize=(10, 6))
plt.plot(k_values, silhouette_scores, 'bo-')
plt.xlabel('Number of Clusters (k)')
plt.ylabel('Silhouette Score')
plt.title('Silhouette Score for K-means Clustering')
plt.show()
```

이 예에서는 `load_iris()`를 사용하여 Iris Dataset를 로드하고 데이터(피처) `X`를 추출한다. 그런 다음 2에서 5 사이의 다양한 `k` 값(클러스터 수)에 대해 k-평균 클러스터링을 수행한다. 각 `k` 값에 대해 `KMeans` 클러스터링 모델을 생성하여 데이터에 적합시키고 객체가 다른 클러스터와 비교하여 자신의 클러스터와 얼마나 유사한지 측정하는 실루엣 점수를 계산한다. 최적의 클러스터 수를 결정하기 위해 `k` 값에 대한 실루엣 점수를 플롯한다.

### Digits Dataset에서 계층적 클러스터링 수행
Digits Dataset은 클러스터링과 분류 작업에 일반적으로 사용되는 손으로 쓴 숫자(0–9)의 데이터세트이다. 각 데이터 포인트는 숫자의 8x8 이미지이다. 여기서는 scikit-learn을 사용하여 이 데이터세트에 대해 계층적 클러스터링을 수행한다.

```python
from sklearn.datasets import load_digits
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import silhouette_score
import matplotlib.pyplot as plt

# Load the Digits Dataset
digits = load_digits()
X = digits.data
# Perform hierarchical clustering with different linkage methods
linkage_methods = ['ward', 'complete', 'average', 'single']
silhouette_scores = []
for linkage in linkage_methods:
    # Create an Agglomerative Clustering model
    model = AgglomerativeClustering(n_clusters=10, linkage=linkage)
    
    # Fit the model to the data
    model.fit(X)
    
    # Calculate the silhouette score
    silhouette_scores.append(silhouette_score(X, model.labels_))
# Plot the silhouette scores for different linkage methods
plt.figure(figsize=(10, 6))
plt.bar(linkage_methods, silhouette_scores)
plt.xlabel('Linkage Method')
plt.ylabel('Silhouette Score')
plt.title('Silhouette Score for Hierarchical Clustering')
plt.show()
```

이 예에서는 `load_digits()`를 사용하여 Digits Dataset를 로드하고 데이터 `X`를 추출한다. 그런 다음 서로 다른 연결 방법(워드, 전체, 평균, 단일)으로 계층적 클러스터링을 수행하고 각 방법에 대한 실루엣 점수를 계산한다. 실루엣 점수는 클러스터링의 품질을 측정하며, 점수가 높을수록 더 잘 정의된 클러스터를 나타낸다. 서로 다른 연결 방법에 대한 실루엣 점수를 플롯하여 성능을 비교한다.

### Wine Dataset에서 DBSAN 클러스터링 수행
Wine Dataset는 클러스터링과 분류 작업에 일반적으로 사용되는 데이터세트이다. 이는 이탈리아의 동일한 지역에서 재배되지만 세 가지 다른 품종에서 파생된 와인에 대한 화학적 분석 결과를 포함하고 있다. 여기서는 이 데이터 세트에 대해 scikit-learn을 사용하여 DBSAN 클러스터링을 수행할 것이다.

```python
from sklearn.datasets import load_wine
from sklearn.cluster import DBSCAN
from sklearn.metrics import silhouette_score
import matplotlib.pyplot as plt

# Load the Wine Dataset
wine = load_wine()
X = wine.data
# Perform DBSCAN clustering with different values of epsilon
eps_values = [0.1, 0.5, 1.0, 1.5, 2.0]
silhouette_scores = []
for eps in eps_values:
    # Create a DBSCAN clustering model
    model = DBSCAN(eps=eps, min_samples=5)
    
    # Fit the model to the data
    model.fit(X)
    
    # Calculate the silhouette score
    silhouette_scores.append(silhouette_score(X, model.labels_))
# Plot the silhouette scores for different values of epsilon
plt.figure(figsize=(10, 6))
plt.plot(eps_values, silhouette_scores, 'bo-')
plt.xlabel('Epsilon')
plt.ylabel('Silhouette Score')
plt.title('Silhouette Score for DBSCAN Clustering')
plt.show()
```

이 예에서는 `load_wine()`을 사용하여 Wine Dataset를 로드하고 데이터 `X`를 추출한다. 그런 다음 서로 다른 값의 엡실론(같은 이웃에서 고려할 두 샘플 사이의 최대 거리를 정의하는 매개 변수)으로 DBSCAN 클러스터링을 수행하고 각 값에 대한 실루엣 점수를 계산한다. 실루엣 점수는 클러스터링의 품질을 측정하며, 점수가 높을수록 더 잘 정의된 클러스터를 나타낸다. DBSCAN 클러스터링에 대한 최적의 값을 결정하기 위해 다양한 값의 엡실론에 대한 실루엣 점수를 플롯한다.

필요에 따라 데이터세트와 모델을 조정하고 scikit-learn을 사용하여 다른 데이터세트에서 클러스터링을 수행하는 데 유사한 단계를 사용할 수 있다.

### Bresst Cancer Dataset에 대한 주성분 분석 수행
Bresst Cancer Dataset는 분류와 클러스터링 작업에 일반적으로 사용되는 데이터세트이다. 유방 종괴의 세침 흡인액(FNA)의 디지털화된 이미지로부터 계산된 피처를 포함한다. 여기서는 scikit-learn을 사용하여 이 데이터 세트에 대해 주성분 분석(PCA)을 수행한다.

```python
from sklearn.datasets import load_breast_cancer
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

# Load the Breast Cancer Dataset
breast_cancer = load_breast_cancer()
X = breast_cancer.data
# Perform PCA to reduce the dimensionality to 2 components for visualization
pca = PCA(n_components=2)
X_pca = pca.fit_transform(X)
# Plot the first two principal components
plt.figure(figsize=(10, 6))
plt.scatter(X_pca[:, 0], X_pca[:, 1], c=breast_cancer.target, cmap='viridis')
plt.xlabel('First Principal Component')
plt.ylabel('Second Principal Component')
plt.title('Principal Component Analysis of Breast Cancer Dataset')
plt.colorbar(label='Target')
plt.show()
```

이 예에서는 `load_breast_cancer()`를 사용하여 Bresst Cancer Dataset를 로드하고 데이터 `X`를 추출한다. 그런 다음 PCA를 수행하여 시각화를 위해 데이터의 차원을 2개의 성분으로 줄인다. 처음 두 개의 주성분은 데이터에서 최대 분산의 방향을 포착한다. 이 두 개의 주성분으로 정의된 2차원 공간에 데이터 포인트를 표시하고 대상 변수(악성의 경우 0, 양성의 경우 1)를 기반으로 색상을 지정한다.

PCA는 고차원 데이터의 차원 축소와 시각화를 위해 자주 사용된다. 데이터의 구조를 이해하고 패턴이나 클러스터를 식별하는 데 도움을 줄 수 있다. 필요에 따라 구성 요소의 수와 시각화를 조정하고 scikit-learn을 사용하여 다른 데이터 세트에서 PCA를 수행하는 데에도 유사한 단계를 적용할 수 있다.

### Boston Housing Dataset에 대한 특이값 분해(singular value decomposition) 수행
Boston Housing Dataset는 회귀 작업에 일반적으로 사용되는 데이터세트이다. 매사추세츠주 보스턴 지역의 주택과 관련하여 미국 센서스 서비스에서 수집한 정보를 포함하고 있다. 여기서는 이 데이터세트에 대해 scikit-learn을 이용하여 SVD(Singular Value Decomposition)를 수행하기로 한다.

```python
from sklearn.datasets import load_boston
from sklearn.decomposition import TruncatedSVD
import matplotlib.pyplot as plt

# Load the Boston Housing Dataset
boston = load_boston()
X = boston.data
# Perform SVD to reduce the dimensionality to 2 components for visualization
svd = TruncatedSVD(n_components=2)
X_svd = svd.fit_transform(X)
# Plot the first two singular components
plt.figure(figsize=(10, 6))
plt.scatter(X_svd[:, 0], X_svd[:, 1], c=boston.target, cmap='viridis')
plt.xlabel('First Singular Component')
plt.ylabel('Second Singular Component')
plt.title('Singular Value Decomposition of Boston Housing Dataset')
plt.colorbar(label='Target')
plt.show()
```

이 예에서는 `load_boston()`을 사용하여 Boston Housing Dataset를 로드하고 데이터 `X`를 추출한다. 그런 다음 SVD를 수행하여 시각화를 위해 데이터의 차원을 2개의 구성 요소로 줄인다. SVD는 행렬을 구성 요소로 분해하는 데 사용되는 방법으로, 데이터 구조를 이해하고 패턴 또는 클러스터를 식별하는 데 도움이 될 수 있다. 이 두 개의 단일 구성 요소로 정의된 2D 공간에 데이터 포인트를 표시하고 대상 변수(1000달러 단위의 소유자 점유 주택의 중위값)를 기반으로 색상을 지정한다.

## <a name="sec_02"></a> 모델 평가와 개선
모델 평가와 개선은 모든 기계 학습 프로젝트의 마지막 단계이다. 기계 학습 모델의 성능과 정확도를 측정하고, 이를 개선하기 위한 다양한 기술을 적용하는 것을 포함한다. 모델 평가와 개선은 문제에 가장 적합한 모델을 선택하고, 파라미터와 하이퍼파라미터를 미세 조정하는 데 도움이 될 수 있다.

scikit-learn은 다음과 같은 모델 평가와 개선을 위한 다양한 도구와 메트릭을 제공한다.

- 교차 검증(Cross-validation)
- 그리드 검색(Grid search)
- 무작위 검색(Randomized search)
- 점수 함수(Scoring functions)
- 혼돈 행렬(Confusion matrix)
- 분류 보고서(Classification report)
- ROC 곡선(ROC curve)
- 정말도-회호 곡선(Precision-recall curve)
- R 제곱 오차(R-squared score)
- 평균 제곱 오차(Mean squared error)
- 평균 절대 오차(Mean absolute error)

이 절에서는 scikit-learn을 사용하여 다음과 같은 일부 모델 평가와 개선 작업을 수행하는 방법에 대해 설명한다.

- Boston Housing Dataset의 선형 회귀 모델 평가
- Iris Dataset에서 로지스틱 회귀 모델 평가
- Breast Cancer Dataset에서 서포트 벡터 분류 모델 평가
- Digits Dataset에서 k-최근접 이웃 분류 모델 평가
- Diabetes Dataset의 의사결정 나무 회귀 모델 평가
- Wine Dataset에서의 랜덤 포레스트 분류 모델 평가
- California Housing Dataset에서의 신경망 회귀 모델 평가

각 작업에 대해 이전 섹션에서 만든 샘플 데이터세트와 모델을 사용한다. 또한 scikit-learn의 `model_selection`과 `metrics` 모듈을 사용하여 모델을 평가하고 개선한다.

첫 번째 과제인 Boston Housing Dataset에서 선형 회귀 모형을 평가하는 것부터 시작한다.

## Boston Housing Dataset의 선형 회귀 모델 평가
먼저 Boston Housing Dataset의 선형 회귀 모델을 평가하는 것으로 시작하겠다. 앞서 훈련한 모델을 사용하여 R-제곱 점수, 평균 제곱 오차, 평균 절대 오차 등의 지표를 사용하여 성능을 평가한다.

```python
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error

# Use the trained linear regression model
y_pred = model_lr.predict(X_test)
# Calculate R-squared score
r2 = r2_score(y_test, y_pred)
print("R-squared Score:", r2)
# Calculate mean squared error
mse = mean_squared_error(y_test, y_pred)
print("Mean Squared Error:", mse)
# Calculate mean absolute error
mae = mean_absolute_error(y_test, y_pred)
print("Mean Absolute Error:", mae)
```

## Iris Dataset에서 로지스틱 회귀 모델 평가
다음으로 Iris Dataset에서 로지스틱 회귀 모델을 평가해 보자. 앞서 훈련한 모델을 사용해 정확도, 정밀도, 회상도, F1-score 등의 지표를 사용해 성능을 평가할 것이다.

```python
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, classification_report

# Use the trained logistic regression model
y_pred = model_logreg.predict(X_test)
# Calculate accuracy
accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)
# Calculate precision, recall, and F1-score
precision = precision_score(y_test, y_pred, average='weighted')
recall = recall_score(y_test, y_pred, average='weighted')
f1 = f1_score(y_test, y_pred, average='weighted')
print("Precision:", precision)
print("Recall:", recall)
print("F1-score:", f1)
# Print classification report
print("Classification Report:")
print(classification_report(y_test, y_pred, target_names=iris.target_names))
```

### Breast Cancer Dataset에서 서포트 벡터 분류 모델 평가
Breast Cancer Dataset의 지원 벡터 분류 모델의 경우 `model_logreg`를 훈련된 지원 벡터 분류 모델 `model_svc`로 대체하여 위와 동일한 접근 방식을 사용할 수 있다.

### Digits Dataset에서 k-최근접 이웃 분류 모델 평가
Digits Dataset의 k-최근접 이웃 분류 모델의 경우 `model_logreg`를 훈련된 k-최근접 이웃 분류 모델 `model_knn`으로 대체하여 위와 동일한 접근 방식을 사용할 수 있다.

### Diabetes Dataset의 의사결정 나무 회귀 모델 평가
Diabetes Dataset의 의사 결정 트리 회귀 모델의 경우 `model_logreg`를 훈련된 의사 결정 트리 회귀 모델 `model_dt`로 대체하여 위와 동일한 접근 방식을 사용할 수 있다.

### Wine Dataset에서의 랜덤 포레스트 분류 모델 평가
Wine Dataset의 랜덤 포레스트 분류 모델의 경우, `model_logreg`를 훈련된 랜덤 포레스트 분류 모델 `model_rf`로 대체하여 위와 동일한 접근 방식을 사용할 수 있다.

### California Housing Dataset에서의 신경망 회귀 모델 평가
California Housing Dataset의 신경망 회귀 모델의 경우 `model_logreg`를 훈련된 신경망 회귀 모델 `model_nn`으로 대체하여 위와 동일한 접근 방식을 사용할 수 있다.

이러한 모델을 각 데이터세트에서 평가함으로써 성능에 대한 통찰력을 얻고 개선해야 할 부분을 식별할 수 있다.

## <a name="summary"></a> 요약
이 포스팅에서는 데이터 분석을 위한 다양한 기계 학습 알고리즘과 도구를 제공하는 Python 라이브러리인 scikit-learn의 사용법을 설명하였다. 데이터 준비와 전처리, 지도와 비지도 학습, 모델 평가와 개선과 같은 일반적인 기계 학습 작업을 수행하는 방법도 보였다.

학습한 요점은 다음과 같다.

- scikit-learn은 NumPy, SciPy, matplotlib 위에 구축되어 있으며, 다양한 기계 학습 알고리즘과 데이터 구조로 작업할 수 있는 일관되고 사용자 친화적인 인터페이스를 제공한다.
- scikit-learn은 선형rhk 로지스틱 회귀, 지원 벡터 머신, 의사 결정 트리, 랜덤 포레스트, k-평균 클러스터링 등 포괄적인 기계 학습 알고리즘 모음을 제공한다.
- scikit-learn은 또한 스케일링, 인코딩, 대치, 교차 검증, 그리드 검색 및 메트릭 같은 데이터 전처리, 특징 추출, 모델 선택 및 평가를 위한 도구를 제공한다.
- scikit-learn은 추정기(estimator), 변환기(transformer) 및 예측(predictor) 객체를 사용하여 서로 다른 기계 학습 모델을 구현하고 조작하며, 이를 적용하고 비교하기 위한 간단하고 일관된 인터페이스를 제공한다.
- scikit-Learn은 희소 데이터 형식과 조밀 데이터 형식을 모두 지원하며, 수치 데이터와 범주 데이터를 모두 처리할 수 있다.
- scikit-learn은 문서화가 잘 되어 있으며 사용자가 라이브러리를 배우고 사용할 수 있도록 많은 예와 튜토리얼을 제공한다.
- scikit-learn은 오픈 소스로 자유롭게 사용할 수 있으며, 개발자와 사용자의 커뮤니티가 크고 활성화되어 있다.

이 포스팅을 학습하면, 여러분은 기계 학습 프로젝트에 scikit-learn을 사용할 수 있는 탄탄한 기반을 갖추게 되었다. 또한 scikit-learn을 사용하여 일부 실제 문제를 해결하는 데 있어서도 실용적인 경험을 쌓았다. 그러나 이 포스팅의 설명이 완전한 것은 아니며 scikit-learn은 더 많은 기능을 지원한다. scikit-learn의 [공식 웹사이트](https://scikit-learn.org/stable/)를 방문하면 더 자세히 살펴볼 수 있으며, 여기서 [문서](), [사용자 가이드](https://scikit-learn.org/stable/user_guide.html), [API 참조](https://scikit-learn.org/stable/modules/classes.html) 및 scikit-learn의 [예](https://scikit-learn.org/stable/auto_examples/index.html)를 찾을 수 있다.
